<?php

namespace Drupal\built_by\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Template\Attribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Built by block.
 * 
 * @Block(
 *  id = "built_by_block",
 *  admin_label = @Translation("Built By Block"),
 * )
 * 
 */

class BuiltByBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * Construct
   * 
   * @param array $configuration
   *  A configuration array containing information about the plugin instance.
   * 
   * @param string $plugin_id
   *  The plugin_id for the plugin instance.
   * 
   * @param string $plugin_definition
   *  The plugin implementation definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return [
      'link' => array(
        'link_text' => 'ChampionsUKPlc',
        'url' => 'http://www.championsukplc.com',
      ),
      'message' => 'Website design and development by',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['message'] = array(
      '#type' => 'textfield',
      '#title' => t('Message'),
      '#description' => t('The message that appears before the link'),
      '#default_value' => $config['message'],
    );

    $form['link'] = array(
      '#type' => 'details',
      '#title' => t('Link'),
      '#open' => TRUE,
      '#description' => t('The link settings'),
    );

    $form['link']['link_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#description' => t('The text that is used for the link'),
      '#default_value' => $config['link']['link_text'],
    );

    $form['link']['url'] = array(
      '#type' => 'url',
      '#title' => t('Link URL'),
      '#description' => t('The URL of the link'),
      '#default_value' => $config['link']['url'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['message'] = $form_state->getValue('message');
    $this->configuration['link']['link_text'] = $form_state->getValue(['link', 'link_text']);
    $this->configuration['link']['url'] = $form_state->getValue(['link', 'url']);
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $config = $this->getConfiguration();

    $link_attributes = new Attribute();

    return [
      '#theme' => 'built_by',
      '#message' => $config['message'],
      '#link_text' => $config['link']['link_text'],
      '#url' => $config['link']['url'],
      '#link_attributes' => $link_attributes,
    ];
  }
}
