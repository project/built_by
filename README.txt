INTRODUCTION
____________

Built By module intends to provide a custom block that can be configured to contain
some text and a link to a website. The functionality here could be achieved
by a simple template, but the addition is that it also includes a check for 
whether you are on the home page or not, and if you are, adds a 'rel=follow'
attribute to the link. On all pages that are not the home page, 'rel=nofollow'
is added.


REQUIREMENTS
____________

Drupal 8 Block Module enabled.


USAGE
_____

On the block layout page, add a new block in the desired region. The following
screen will ask you for configuration which allows you to modify the message.
There is also a template that can be overwritten if you wish to add styling.
